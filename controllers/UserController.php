<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\GameSearch;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for Users model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['register'],
                        'allow' => false, // Has access
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true, // Has access
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['register'],
                        'allow' => true, // Do not have access
                        'roles'=>['?'], // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionRegister()
    {
        //Yii::$app->user->identity
        $model = new Users();
        $model->setscenario('insert');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            date_default_timezone_set('Asia/Jakarta');

            $file1 = UploadedFile::getInstance($model, 'file');
            if ($file1 != null){  
                $model->Image = 'img_'.date('dmY').'_'.date('His').'.'.$file1->extension;
                $model->file = $file1;
            }

            $model->DateOfBirth = date("Y-m-d", strtotime($model->DateOfBirth));
            if ($model->Password != $model->varPassword){
                //reset password
                $model->Password = "";
                $model->varPassword = "";

                //set error message
                $model->addError('varPassword', 'Password confirm must be the same as password');
                $model->DateOfBirth = date("d-M-Y", strtotime($model->DateOfBirth));
                $model->file = null;

                return $this->render('register', [
                    'model' => $model,
                ]);
            }

            $model->Password = crypt($model->Password);

            if ($model->save()){

                if ($file1 != null){
                    $model->file->saveAs('img/' . $model->Image);
                }

                Yii::$app->user->login($model, 0);
                return $this->redirect(['site/index']);
                
            }else{
                $model->file = null;
                $model->DateOfBirth = date("d-M-Y", strtotime($model->DateOfBirth));
                return $this->render('register', [
                    'model' => $model,
                ]);   
            }
        } else {
            $model->file = null;
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }

    public function actionProfile()
    {
        $id = Yii::$app->user->identity->UserId;
        $model = $this->findModel($id);

        $searchModel = new GameSearch();
        $params = Yii::$app->request->queryParams;
        if (isset($params["search_query"])){
            $searchModel->search_query = $params["search_query"];
        }
        if (isset($params["order_by"])){
            $searchModel->order_by = $params["order_by"];
        }

        $searchModel->owner = $id;

        $dataProvider = $searchModel->search($params);

        return $this->render('profile', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $id = Yii::$app->user->identity->UserId;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            date_default_timezone_set('Asia/Jakarta');

            $file1 = UploadedFile::getInstance($model, 'file');
            if ($file1 != null){  
                $model->Image = 'img_'.date('dmY').'_'.date('His').'.'.$file1->extension;
                $model->file = $file1;
            }

            $model->DateOfBirth = date("Y-m-d", strtotime($model->DateOfBirth));

            if ($model->save()){
                if ($file1 != null){
                    $model->file->saveAs('img/' . $model->Image);
                }

                return $this->redirect(['profile']);   
            }else{
                $model->DateOfBirth = date("d-M-Y", strtotime($model->DateOfBirth));
                $model->file = null;
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            $model->file = null;
            $model->DateOfBirth = date("d-M-Y", strtotime($model->DateOfBirth));
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionChangepassword()
    {
        $id = Yii::$app->user->identity->UserId;
        $model = $this->findModel($id);
        $model->setscenario('change');

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->validatePassword($model->curPassword)){
                $model->curPassword = "";
                $model->varPassword = "";
                $model->newPassword = "";
                $model->addError('curPassword', 'Your current Password is incorrect');
                return $this->render('changepassword', [
                    'model' => $model,
                ]);
            }

            if ($model->newPassword != $model->varPassword){
                $model->curPassword = "";
                $model->varPassword = "";
                $model->newPassword = "";
                $model->addError('varPassword', 'Password confirm must be the same as new password');
                return $this->render('changepassword', [
                    'model' => $model,
                ]);
            }

            $model->Password = crypt($model->newPassword);

            if ($model->save()){
                return $this->redirect(['profile']);
            }

        } else {
            return $this->render('changepassword', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
