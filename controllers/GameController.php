<?php

namespace app\controllers;

use Yii;
use app\models\Games;
use app\models\Comments;
use app\models\GameSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\filters\AccessControl;

/**
 * GameController implements the CRUD actions for Games model.
 */
class GameController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true, // Has access
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view', 'getcomment'],
                        'allow' => true, // Do not have access
                        'roles'=>['?'], // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Games models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GameSearch();
        $params = Yii::$app->request->queryParams;
        if (isset($params["search_query"])){
            $searchModel->search_query = $params["search_query"];
        }
        if (isset($params["order_by"])){
            $searchModel->order_by = $params["order_by"];
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Games model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Games model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Games();
        $model->setscenario('insert');

        if ($model->load(Yii::$app->request->post())) {
            $model->AuthorId = Yii::$app->user->identity->UserId;
            $model->PostDate = new \yii\db\Expression('NOW()');

            $file1 = UploadedFile::getInstance($model, 'file');
            $model->file = $file1;

            date_default_timezone_set('Asia/Jakarta');
            
            $model->Image = 'img_'.date('dMY').'_'.date('His').'.'.$file1->extension;

                
            if ($model->save()){
                $model->file->saveAs('img/' . $model->Image); 

                return $this->redirect(['view', 'id' => $model->GameId]);   
            }else{
                return $this->render('create', [
                    'model' => $model,
                ]);    
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Games model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            date_default_timezone_set('Asia/Jakarta');

            $file1 = UploadedFile::getInstance($model, 'file');
            if ($file1 == null && $model->Image == ""){
                $model->addError('file', 'Please upload a file.');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }

            if ($file1 != null){  
                $model->Image = 'img_'.date('dmY').'_'.date('His').'.'.$file1->extension;
                $model->file = $file1;
            }

            if ($model->save()){
                if ($file1 != null){
                    $model->file->saveAs('img/' . $model->Image);
                }
                return $this->redirect(['view', 'id' => $model->GameId]);   
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Games model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Games model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Games the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Games::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPostcomment()
    {
        $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND 
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
        
        if (!$isAjax)
            throw new NotFoundHttpException('The requested page does not exist.');

        date_default_timezone_set('Asia/Jakarta');

        if (isset($_POST["game"]) && isset($_POST["comment"])){
            $model = new Comments();
            $model->GameId = $_POST["game"];
            $model->AuthorId = Yii::$app->user->identity->UserId;
            $model->Comment = $_POST["comment"];
            $model->PostDate = new \yii\db\Expression('NOW()');

            $model->save();

            $model = Comments::findOne($model->CommentId);

            return json_encode([
                'comment' => $model->Comment, 
                'id' => $model->CommentId, 
                'author' => $model->author->Firstname.' '.$model->author->Lastname, 
                'date' => $this->dateFormat($model->PostDate),
                'image' => $model->author->Image == null || $model->author->Image == "" ? 'default-pp.jpg' : $model->author->Image
            ]);
        }else{
            return [];
        }
    }

    public function actionGetcomment()
    {
        date_default_timezone_set('Asia/Jakarta');

        $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND 
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
        
        if (!$isAjax || !isset($_GET['game']))
            throw new NotFoundHttpException('The requested page does not exist.');

        $model = new Comments();

        if (isset($_GET['id']) && $_GET['id'] > 0){
            $model = Comments::find()
                ->where('CommentId < :id', [':id' => $_GET['id']])
                ->andWhere('GameId = :game', [':game' => $_GET['game']])
                ->orderBy([ 'Postdate' => SORT_DESC ])
                ->limit(5)->all();
        }else{
            $model = Comments::find()
                ->where('GameId = :game', [':game' => $_GET['game']])
                ->orderBy([ 'Postdate' => SORT_DESC ])
                ->limit(5)
                ->all();   
        }

        $output = [];
        $minid = -1;

        foreach($model as $row){
            if ($minid == -1 || $minid > $row->CommentId){
                $minid = $row->CommentId;
            }
        }

        $comment_count = Comments::find()
            ->where('CommentId < :id', [':id' => $minid])
            ->andWhere('GameId = :game', [':game' => $_GET['game']])
            ->count();

        foreach($model as $row){
            array_push($output, [
                'comment' => $row->Comment, 
                'count' => $comment_count, 
                'id' => $row->CommentId, 
                'author' => $row->author->Firstname.' '.$row->author->Lastname, 
                'date' => $this->dateFormat($row->PostDate),
                'image' => $row->author->Image == null || $row->author->Image == "" ? 'default-pp.jpg' : $row->author->Image
            ]);
        }

        return json_encode($output);
    }

    function dateFormat($date){
        date_default_timezone_set('Asia/Jakarta');

        if (date('dmY') == date("dmY", strtotime($date))){
            return date("h:i A", strtotime($date)). ' Today';
        }else if (date('dmY') == date("dmY", strtotime('+1 day', strtotime($date)))){
            return date("h:i A", strtotime($date)). ' Yesterday';
        }else{
            return date("h:i A M d, Y", strtotime($date));
        }
    }
}
