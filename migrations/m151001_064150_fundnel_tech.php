<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_064150_fundnel_tech extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Create Table

        $this->createTable('{{%Users}}', [
            'UserId' => Schema::TYPE_PK,
            'Email' => 'VARCHAR(50) NOT NULL',
            'Username' => 'VARCHAR(50) NOT NULL',
            'Firstname' => 'VARCHAR(150) NOT NULL',
            'Lastname' => 'VARCHAR(150) NOT NULL',
            'Password' => 'VARCHAR(150) NOT NULL',
            'DateOfBirth' => 'DATE NOT NULL',
            'Image' => 'VARCHAR(500)'
        ], $tableOptions);

        $this->createTable('{{%Games}}', [
            'GameId' => Schema::TYPE_PK,
            'Name' => 'VARCHAR(150) NOT NULL',
            'Description' => 'TEXT NOT NULL',
            'Image' => 'VARCHAR(500) NOT NULL',
            'AuthorId' => 'INT NOT NULL',
            'PostDate' => 'DATETIME NOT NULL',
            'Review' => 'VARCHAR(500) NOT NULL'
        ], $tableOptions);

        $this->createTable('{{%Comments}}', [
            'CommentId' => Schema::TYPE_PK,
            'GameId' => 'INT NOT NULL',
            'AuthorId' => 'INT NOT NULL',
            'Comment' => 'VARCHAR(500) NOT NULL',
            'PostDate' => 'DATETIME NOT NULL'
        ], $tableOptions);


        //Foreign Key

        //Table Games
        $this->addForeignKey("fk_game_author_id", "Games", "AuthorId", "Users", "UserId");
        //Table Comments
        $this->addForeignKey("fk_comment_game_id", "Comments", "GameId", "Games", "GameId");
        $this->addForeignKey("fk_comment_author_id", "Comments", "AuthorId", "Users", "UserId");
    }

    public function down()
    {
        //Drop Foreign Key
        
        $this->dropForeignKey("fk_game_author_id", "Games");
        $this->dropForeignKey("fk_comment_game_id", "Comments");
        $this->dropForeignKey("fk_comment_author_id", "Comments");

        //Drop Table

        $this->dropTable('{{%Users}}');
        $this->dropTable('{{%Games}}');
        $this->dropTable('{{%Comments}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
