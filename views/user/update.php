<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Update User Profile';
$this->params['breadcrumbs'][] = ['label' => 'My Pages', 'url' => ['profile']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="users-update">
<div class="box box-default">
<div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
