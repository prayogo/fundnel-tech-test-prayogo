<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(['options'=>['class'=>'form-horizontal', 'enctype' => 'multipart/form-data']]); ?>

    <div class="box-body">

    <?php 
        if ($model->isNewRecord){
            echo $form->field($model, 'Email', [
                'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                'labelOptions'=>['class'=>'col-sm-2 control-label'],
            ])->textInput(['maxlength' => true]);
        }else{
            echo '<div class="form-group">';
            echo Html::activeLabel($model, 'Email', ['class'=>'col-sm-2 control-label']);
            echo '<div class="col-sm-10" style="margin-bottom:10px"><span class="form-control">'.$model->Email.'</span></div></div>';
            
        }
    ?>

    <?php
    
        if ($model->isNewRecord){
            echo $form->field($model, 'Username', [
                'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                'labelOptions'=>['class'=>'col-sm-2 control-label']
            ])->textInput(['maxlength' => true]);
        }else{
            echo '<div class="form-group">';
            echo Html::activeLabel($model, 'Username', ['class'=>'col-sm-2 control-label']);
            echo '<div class="col-sm-10" style="margin-bottom:10px"><span class="form-control">'.$model->Username.'</span></div></div>';
            
        }
    

    ?>

    <?= $form->field($model, 'Firstname', [
        'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
        'labelOptions'=>['class'=>'col-sm-2 control-label']
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Lastname', [
        'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
        'labelOptions'=>['class'=>'col-sm-2 control-label']
    ])->textInput(['maxlength' => true]) ?>

    <?php
        if ($model->isNewRecord){
            echo $form->field($model, 'Password', [
                'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                'labelOptions'=>['class'=>'col-sm-2 control-label']
            ])->passwordInput(['maxlength' => true]);
            echo $form->field($model, 'varPassword', [
                'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                'labelOptions'=>['class'=>'col-sm-2 control-label']
            ])->passwordInput(['maxlength' => true]);
        }
    ?>

    <?php
        echo $form->field($model, 'DateOfBirth', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions'=>['class'=>'col-sm-2 control-label']
        ])->widget(
            DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter date of birth..'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'd-M-yyyy'
                ]
            ]
        );
    ?>

    <?= $form->field($model, 'file', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions'=>['class'=>'col-sm-2 control-label'],
            'inputOptions'=>['style'=>'margin-top:5px']
        ])->fileInput() ?> 

    </div>

    <div class="box-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Register' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .form-horizontal .form-group{
        margin-bottom:5px;
    }
</style>