<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-register">
<div class="box box-default">
	<?php $form = ActiveForm::begin(['options'=>['class'=>'form-horizontal', 'enctype' => 'multipart/form-data']]); ?>
	
	<div class="box-body">	
    	<?php
    		
    		echo $form->field($model, 'curPassword', [
                'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                'labelOptions'=>['class'=>'col-sm-2 control-label']
            ])->passwordInput(['maxlength' => true]);
    		echo $form->field($model, 'newPassword', [
                'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                'labelOptions'=>['class'=>'col-sm-2 control-label']
            ])->passwordInput(['maxlength' => true]);
            echo $form->field($model, 'varPassword', [
                'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                'labelOptions'=>['class'=>'col-sm-2 control-label']
            ])->passwordInput(['maxlength' => true]);

    	?>

		<div class="box-footer">
	        <?= Html::submitButton($model->isNewRecord ? 'Register' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
	    </div>

    </div>

    <?php ActiveForm::end(); ?>
</div>

</div>
