<?php

use yii\helpers\Html;
use yii\widgets\ListView;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'My Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-register">
	
<div class="box-body">
<div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-tools pull-right">
					<?= Html::a('<i class="fa fa-pencil"></i>', ['update'], [
					    'class' => 'btn btn-box-tool',
					    'data-toggle'=>'tooltip',
					    'title'=>'Edit',
					]) ?>
				</div>
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="<?= \Yii::$app->request->BaseUrl; ?>/img/<?= Yii::$app->user->identity->Image == null || Yii::$app->user->identity->Image == "" ? 'default-pp.jpg' : Yii::$app->user->identity->Image ?>" alt="User profile picture">
                  <h3 class="profile-username text-center"><?= $model->Firstname . ' ' . $model->Lastname ?></h3>
                  <p class="text-muted text-center"><?= $model->Email ?></p>

                  <ul class="list-group list-group-unbordered">
                  	<li class="list-group-item">
                      <b>Games</b> <a class="pull-right"><?= count($model->games) ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Comments</b> <a class="pull-right"><?= count($model->comments) ?></a>
                    </li>
                  </ul>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">About Me</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <strong><i class="fa fa-birthday-cake margin-r-5"></i>  Born</strong>
                  <p class="text-muted">
                    <?= date("F d, Y", strtotime($model->DateOfBirth)) ?>
                  </p>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post">
                      <div class="box-header">
      <div class="box-tools">
<?= Html::beginForm('profile', 'GET', ['id'=>'form-search']) ?>

    <div class="input-group" style="width: 350px;">

            <?php 
                echo Html::activeTextInput($searchModel, 'search_query', [
                        'maxlength' => 150, 'placeholder'=>'Search',
                        'class'=>'form-control input-sm pull-right',
                        'style'=>'width:160px',
                        'name'=>'search_query'
                ]); 
            ?>

            <div class="input-group-btn">
                <button class="btn btn-sm btn-default" style="z-index: 2;margin-left: -1px;border-top-left-radius: 0;
                    border-bottom-left-radius: 0;"><i class="fa fa-search"></i></button>
            </div>

            <?php 
                echo Html::activeDropDownList($searchModel, 'order_by', 
                [ 'na'=>'Name (a - z)', 'nd'=>'Name (z - a)', 'dd'=>'Date (newest - oldest)', 'da'=>'Date (oldest - newest)' ],
                [
                    'class'=>'form-control input-sm', 'style'=>'margin-left:5px;',
                    'id'=>'sort_option', 'name'=>'order_by'
                ]); 
            ?>
    
        </div>
<?= Html::endForm() ?>
      </div>
    </div>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
                //return Html::a(Html::encode($model->Name), ['view', 'id' => $model->GameId]);
                $str = $model->Description;
                if (strlen($model->Description) > 260){
                    $str = substr($str, 0, 257) . '...';   
                }
                return '<div class="attachment-block clearfix">
                            <img class="attachment-img" src="'.\Yii::$app->request->BaseUrl.'/img/'.$model->Image.'" alt="attachment image">
                            <div class="attachment-pushed">
                              <h4 class="attachment-heading">'.Html::a(Html::encode($model->Name), ['game/view', 'id' => $model->GameId]).'</h4>
                              <div class="attachment-text attachment-body">'.$str.'</div>
                              <div class="attachment-text pull-right">
                                    <i>'.$model->author->Firstname.' '.$model->author->Lastname.', '.date("M d, Y H:i", strtotime($model->PostDate)).'</i>
                              </div>
                            </div>
                          </div>';
            },
    ]) ?>
                    </div><!-- /.post -->
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div>
</div>

</div>

<?php    

$this->registerJs('
    $( document ).ready(function() {
        $("#sort_option").change(function (e){
            $("#form-search").submit();
        });

        var search = $("#gamesearch-search_query").val();
        if (search.trim() != ""){
            $(".attachment-heading a").each(function (index, item){
                var innerText = $(item).text();
                $(item).text("").append(highlight(innerText, search));
            });

            $(".attachment-body").each(function (index, item){
                var innerText = $(item).text();
                $(item).text("").append(highlight(innerText, search));
            });
        }

        
    });

    function highlight(html, search){
        if (html.toLowerCase().indexOf(search.toLowerCase()) >= 0)
            html = html.replace(new RegExp( "(" + search + ")", "gi" ), "<mark>$1</mark>" )
        else
            html = html.replace("...","<mark>...</mark>");

        return html;
    }
')

?>

<style>
    .attachment-text, .attachment-footer{
        font-size:11px;
    }
</style>