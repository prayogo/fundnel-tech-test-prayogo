<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Register User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-register">
<div class="box box-default">
	<div class="box-body">
	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    </div>
</div>

</div>
