<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = 'Game Review';
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->Name;
?>
<div class="games-view">

<div class="box box-default">

    <div class="box box-widget" style="margin-top:1px">
            
            <div class="box box-widget widget-user" style="margin-bottom:0px">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-black" style="background: linear-gradient( rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3) ), 
                url('<?= \Yii::$app->request->BaseUrl; ?>/img/<?= $model->Image ?>') center center; height:350px;background-size: cover;
                background-position-y: -50px;">

                    <h1 style="text-align: center;"><?= $model->Name ?></h1>
                    <h5 style="text-align: center;"><?= nl2br($model->Review) ?></h5>
                </div>

                

              </div>
              
                <div class="box-header with-border">
                  <div class="user-block">
                    <img class="img-circle" src="<?= \Yii::$app->request->BaseUrl; ?>/img/<?= $model->author->Image == null || $model->author->Image == "" ? 'default-pp.jpg' : $model->author->Image ?>" alt="user image">
                    <span class="username"><a href="#"><?= $model->author->Firstname . ' ' . $model->author->Lastname ?></a></span>
                    <span class="description"><?= date("M d, Y H:i", strtotime($model->PostDate)) ?></span>
                  </div><!-- /.user-block -->
<?php if (isset(Yii::$app->user->identity->Username) && $model->AuthorId == Yii::$app->user->identity->UserId){ ?>
                  <div class="box-tools pull-right">
<?= Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->GameId], [
    'class' => 'btn btn-box-tool',
    'data-toggle'=>'tooltip',
    'title'=>'Edit'
]) ?>
<?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->GameId], [
    'class' => 'btn btn-box-tool',
    'data-toggle'=>'tooltip',
    'title'=>'Delete',
    'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
    ],
]) ?>
                  </div>
<?php } ?>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <!-- post text -->
                  <p><?= nl2br($model->Description) ?></p>
                  <span class="pull-right text-muted" style="margin-right:5px;display:none" id="span-more-comment">
                    <a href="#" id="more-comment">View <span id="comment-count"></span> more comments</a></span>
                </div><!-- /.box-body -->
                <div class="box-footer box-comments" id="comments">
                    <span id="comment-note">Be the first to comment...</span>
                </div><!-- /.box-footer -->
                <?php if (isset(Yii::$app->user->identity->Username)){ ?>
                <div class="box-footer">
                  <form id="form-comment">
                    <img class="img-responsive img-circle img-sm" src="<?= \Yii::$app->request->BaseUrl; ?>/img/<?= Yii::$app->user->identity->Image == null || Yii::$app->user->identity->Image == "" ? 'default-pp.jpg' : Yii::$app->user->identity->Image ?>" alt="alt text">
                    <!-- .img-push is used to add margin to elements next to floating images -->
                    <div class="img-push">
                      <input id="CommentInput" type="text" class="form-control input-sm" placeholder="Press enter to post comment">
                    </div>
                  </form>
                </div><!-- /.box-footer -->
                <?php } ?>
              </div>

</div>
</div>
<?php

$this->registerJs('
    function handleSubmit() {
        var data = {
            "comment": $("#CommentInput").val(),
            "game": '.$model->GameId.',
        };

        postComment(data);

        return false;
    }

    function postComment(data) {
        $.ajax({
            type: "POST",
            url: "'.yii\helpers\Url::toRoute('game/postcomment').'",
            data: data,
            headers: {
              "X-Requested-With": "XMLHttpRequest"
            },
            success: postSuccess,
            error: postError
        });
    }

    function postError(jqXHR, textStatus, errorThrown){
        console.log(jqXHR);
        alert("An error occurred while posting your comment. Please try again later.");
    }

    function postSuccess(data, textStatus, jqXHR) {
        if (data == ""){
            alert("An error occurred while posting your comment. Please try again later.");
            return;
        }

        $("#form-comment").get(0).reset();
        $("#comment-note").remove();

        data = JSON.parse(data);

        var html = commentHtml(data);
        $("#comments").append($(html).fadeIn("normal"));
    }

    function loadComment(id){
        id = id || 0;
        $.ajax({
            type: "GET",
            url: "'.yii\helpers\Url::toRoute('game/getcomment').'",
            data: {"id":id,"game":'.$model->GameId.'},
            headers: {
              "X-Requested-With": "XMLHttpRequest"
            },
            success: getSuccess,
            error: getError
        });
    }

    function getError(jqXHR, textStatus, errorThrown){
        alert("An error occurred while loading comments. Please try again later.");
    }

    function getSuccess(data, textStatus, jqXHR){
        data = JSON.parse(data);

        if (data.length < 1){
            $("#span-more-comment").hide();
            return;
        }
        
        $("#comment-note").remove();
        $("#comment-count").text(data[0].count);
        if (data[0].count == 0){
            $("#span-more-comment").hide();
        }else{
            $("#span-more-comment").show();
        }

        for(var i = 0; i < data.length; i++){    
            if ($(\'input.asdklfjwiersdfb[value="\'+data[i].id+\'"]\').length == 0){
                var html = commentHtml(data[i]);
                $("#comments").prepend($(html).fadeIn("normal"));   
            }
        }
    }

    function commentHtml(data){
        var html = \'\'+
            \'<div class="box-comment">\'+
                \'<input type="hidden" class="asdklfjwiersdfb" value="\'+data.id+\'"/>\'+
                \'<img class="img-circle img-sm" src="'.\Yii::$app->request->BaseUrl.'/img/\'+data.image+\'" alt="user image">\'+
                \'<div class="comment-text">\'+
                    \'<span class="username">\'+
                        data.author+
                        \'<span class="text-muted pull-right">\'+data.date+\'</span>\'+
                    \'</span>\'+ data.comment +
                \'</div>\'+
            \'</div>\';

        return html;
    }

    $( document ).ready(function() {
        $("#form-comment").submit(handleSubmit);

        loadComment();

        $("#more-comment").click(function(e){
            var lastid = $("input.asdklfjwiersdfb").clone().sort(function(a, b) {
                return parseInt($(a).attr("value")) - parseInt($(b).attr("value"));
            }).first().attr("value");

            loadComment(lastid);

            return false;
        });
    });

    

');

?>
<style>
    #more-comment:hover{
        text-decoration: underline;
    }
</style>