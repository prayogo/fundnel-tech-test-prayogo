<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = 'Update Games';
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->GameId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="games-update">
<div class="box box-default">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>
