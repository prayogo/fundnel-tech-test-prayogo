<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\helpers\BaseHtml;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="games-index">

<div class="box box-default">

<div class="box-body">

    <div class="box-header">
      <div class="box-tools">
<?= Html::beginForm('index', 'GET', ['id'=>'form-search']) ?>
    <div class="input-group" style="width: 350px;">

            <?php 
                echo Html::activeTextInput($searchModel, 'search_query', [
                        'maxlength' => 150, 'placeholder'=>'Search',
                        'class'=>'form-control input-sm pull-right',
                        'style'=>'width:160px',
                        'name'=>'search_query'
                ]); 
            ?>

            <div class="input-group-btn">
                <button class="btn btn-sm btn-default" style="z-index: 2;margin-left: -1px;border-top-left-radius: 0;
                    border-bottom-left-radius: 0;"><i class="fa fa-search"></i></button>
            </div>

            <?php 
                echo Html::activeDropDownList($searchModel, 'order_by', 
                [ 'na'=>'Name (a - z)', 'nd'=>'Name (z - a)', 'dd'=>'Date (newest - oldest)', 'da'=>'Date (oldest - newest)' ],
                [
                    'class'=>'form-control input-sm', 'style'=>'margin-left:5px;',
                    'id'=>'sort_option', 'name'=>'order_by'
                ]); 
            ?>
    
        </div>
<?= Html::endForm() ?>
      </div>
    </div>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
                //return Html::a(Html::encode($model->Name), ['view', 'id' => $model->GameId]);
                $str = $model->Description;
                if (strlen($model->Description) > 380){
                    $str = substr($str, 0, 377) . '...';   
                }
                return '<div class="attachment-block clearfix">
                            <img class="attachment-img" src="'.\Yii::$app->request->BaseUrl.'/img/'.$model->Image.'" alt="attachment image">
                            <div class="attachment-pushed">
                              <h4 class="attachment-heading">'.Html::a(Html::encode($model->Name), ['view', 'id' => $model->GameId]).'</h4>
                              <div class="attachment-text attachment-body">'.$str.'</div>
                              <div class="attachment-text pull-right">
                                    <i>'.$model->author->Firstname.' '.$model->author->Lastname.', '.date("M d, Y H:i", strtotime($model->PostDate)).'</i>
                              </div>
                            </div>
                          </div>';
            },
    ]) ?>

</div>

</div>

</div>

<style>
    .attachment-text, .attachment-footer{
        font-size:11px;
    }
</style>


<?php    

$this->registerJs('
    $( document ).ready(function() {
        $("#sort_option").change(function (e){
            $("#form-search").submit();
        });

        var search = $("#gamesearch-search_query").val();
        if (search.trim() != ""){
            $(".attachment-heading a").each(function (index, item){
                var innerText = $(item).text();
                $(item).text("").append(highlight(innerText, search));
            });

            $(".attachment-body").each(function (index, item){
                var innerText = $(item).text();
                $(item).text("").append(highlight(innerText, search));
            });
        }

        
    });

    function highlight(html, search){
        if (html.toLowerCase().indexOf(search.toLowerCase()) >= 0)
            html = html.replace(new RegExp( "(" + search + ")", "gi" ), "<mark>$1</mark>" )
        else
            html = html.replace("...","<mark>...</mark>");

        return html;
    }
')

?>