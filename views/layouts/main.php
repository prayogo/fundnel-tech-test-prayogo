<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl; ?>/dist/css/skins/skin-blue.min.css">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl; ?>/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl; ?>/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl; ?>/ionicons-2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl; ?>/dist/css/AdminLTE.min.css">
</head>
<body class="skin-blue layout-top-nav">
<?php $this->beginBody() ?>

<div class="wrapper">
    <header class="main-header">

        <nav class="navbar navbar-static-top" role="navigation">
        
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= \Yii::$app->request->BaseUrl; ?>">Fundnel Tech</a>
        </div>
        
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              
              <!-- Tasks Menu -->
<?php if (!Yii::$app->user->isGuest) { ?>
              <li><?= Html::a('<i class="fa fa-plus-circle"></i> New Game', ['/game/create']) ?></li>
<?php } ?>
              <li><?= Html::a('Home', ['/site/index']) ?></li>
              <li><?= Html::a('Games', ['/game/index']) ?></li>
<?php if (Yii::$app->user->isGuest) { ?>
              <li><?= Html::a('Register', ['/user/register']) ?></li>
              <li><?= Html::a('Login', ['/site/login']) ?></li>
<?php } ?>
<?php if (!Yii::$app->user->isGuest) { ?>
              <li><?= Html::a('My Pages', ['/user/profile']) ?></li>
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="<?= \Yii::$app->request->BaseUrl; ?>/img/<?= Yii::$app->user->identity->Image == null || Yii::$app->user->identity->Image == "" ? 'default-pp.jpg' : Yii::$app->user->identity->Image ?>" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?= Yii::$app->user->identity->Firstname . ' ' . Yii::$app->user->identity->Lastname ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="<?= \Yii::$app->request->BaseUrl; ?>/img/<?= Yii::$app->user->identity->Image == null || Yii::$app->user->identity->Image == "" ? 'default-pp.jpg' : Yii::$app->user->identity->Image ?>" class="img-circle" alt="User Image">
                    <p>
                      <?= Yii::$app->user->identity->Firstname . ' ' . Yii::$app->user->identity->Lastname ?>
                      <small><?= Yii::$app->user->identity->Email ?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <?= Html::a('Change password', ['/user/changepassword'], ['class'=>'btn btn-default btn-flat']) ?>
                    </div>
                    <div class="pull-right">
                      <?= Html::a('Sign out', ['/site/logout'],['data-method' => 'post','class'=>'btn btn-default btn-flat']) ?>
                    </div>
                  </li>
                </ul>
              </li>
<?php } ?>
              
            </ul>
          </div>
        </nav>
    </header>
    <div class="content-wrapper">
        <div class="container">
            <section class="content-header">
                <h1><?= $this->title ?></h1>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </section>
            <section class="content">
            <?= $content ?>
                
            </section>
        </div>
    </div>

    <footer class="main-footer">
        <div class="container">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.0
            </div>
            <strong>Copyright © 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
        </div><!-- /.container -->
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<!-- jQuery 2.1.4 -->
<!--<script src="<?= \Yii::$app->request->BaseUrl; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>-->
<!-- Bootstrap 3.3.5 -->
<script src="<?= \Yii::$app->request->BaseUrl; ?>/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= \Yii::$app->request->BaseUrl; ?>/dist/js/app.min.js"></script>
