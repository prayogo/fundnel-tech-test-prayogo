<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "games".
 *
 * @property integer $GameId
 * @property string $Name
 * @property string $Description
 * @property string $Image
 * @property integer $AuthorId
 * @property string $PostDate
 * @property string $Review
 *
 * @property Comments[] $comments
 * @property Users $author
 */
class Games extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'Image', 'AuthorId', 'PostDate', 'Review'], 'required'],
            [['Description'], 'string'],
            [['AuthorId'], 'integer'],
            [['PostDate'], 'safe'],
            [['Name'], 'string', 'max' => 150],
            [['Image'], 'string', 'max' => 500],
            [['Review'], 'string', 'max' => 500],
            [['file'],'safe'],
            [['file'], 'file', 'skipOnEmpty' => false, 'on' => 'insert'],
            [['AuthorId'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['AuthorId' => 'UserId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'GameId' => 'Game ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'Image' => 'Image',
            'file' => 'Image',
            'AuthorId' => 'Author ID',
            'PostDate' => 'Post Date',
            'Review' => 'Review',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['GameId' => 'GameId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Users::className(), ['UserId' => 'AuthorId']);
    }
}
