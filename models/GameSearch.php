<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Games;

/**
 * GameSearch represents the model behind the search form about `app\models\Games`.
 */
class GameSearch extends Games
{
    /**
     * @inheritdoc
     */

    public $order_by = "dd";
    public $search_query = "";
    public $owner = "";

    public function rules()
    {
        return [
            [['GameId', 'AuthorId'], 'integer'],
            [['Name', 'Description', 'Image', 'PostDate', 'Review'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Games::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array('pageSize' => 10),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->where(
            ['like', 'Name', $this->search_query]
        );

        $query->orWhere(
            ['like', 'Description', $this->search_query]
        );

        if ($this->owner != ""){
            $query->andWhere('AuthorId = :1', [':1' => $this->owner]);   
        }

        if ($this->order_by == "na"){
            $query->orderBy(
                ['Name' => SORT_ASC]
            );
        }else if ($this->order_by == "nd"){
            $query->orderBy(
                ['Name' => SORT_DESC]
            );
        }else if ($this->order_by == "da"){
            $query->orderBy(
                ['PostDate' => SORT_ASC]
            );
        }else if ($this->order_by == "dd"){
            $query->orderBy(
                ['PostDate' => SORT_DESC]
            );
        }

        return $dataProvider;
    }
}
