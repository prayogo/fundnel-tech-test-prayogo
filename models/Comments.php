<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property integer $CommentId
 * @property integer $GameId
 * @property integer $AuthorId
 * @property string $Comment
 * @property string $PostDate
 *
 * @property Users $author
 * @property Games $game
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['GameId', 'AuthorId', 'Comment', 'PostDate'], 'required'],
            [['GameId', 'AuthorId'], 'integer'],
            [['PostDate'], 'safe'],
            [['Comment'], 'string', 'max' => 500],
            [['AuthorId'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['AuthorId' => 'UserId']],
            [['GameId'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['GameId' => 'GameId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CommentId' => 'Comment ID',
            'GameId' => 'Game ID',
            'AuthorId' => 'Author ID',
            'Comment' => 'Comment',
            'PostDate' => 'Post Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Users::className(), ['UserId' => 'AuthorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['GameId' => 'GameId']);
    }
}
