<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;


/**
 * This is the model class for table "users".
 *
 * @property integer $UserId
 * @property string $Email
 * @property string $Username
 * @property string $Firstname
 * @property string $Lastname
 * @property string $Password
 * @property string $DateOfBirth
 * @property string $Image 
 *
 * @property Comments[] $comments
 * @property Games[] $games
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */

    public $file;

    public $varPassword;
    public $curPassword;
    public $newPassword;

    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Email', 'Username', 'Firstname', 'Lastname', 'Password', 'DateOfBirth'], 'required'],
            [['DateOfBirth'], 'safe'],
            [['Email', 'Username'], 'string', 'max' => 50],
            [['Firstname', 'Lastname', 'Password'], 'string', 'max' => 150],
            [['Email'],'email'],
            [['Username'], 'unique'],
            [['Email'], 'unique'],
            [['varPassword'], 'required', 'on' => 'insert'],
            [['curPassword', 'varPassword', 'newPassword'], 'required', 'on' => 'change'],
            [['file'],'safe'],
            [['Image'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'UserId' => 'User ID',
            'Email' => 'Email',
            'Username' => 'Username',
            'Firstname' => 'First Name',
            'Lastname' => 'Last Name',
            'Password' => 'Password',
            'DateOfBirth' => 'Date Of Birth',
            'varPassword' => 'Confirm Password',
            'newPassword' => 'New Password',
            'curPassword' => 'Current Password',
            'file' => 'Profile Picture',
            'Image' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['AuthorId' => 'UserId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Games::className(), ['AuthorId' => 'UserId']);
    }

    public function validatePassword($password)
    {
        if (crypt($password, $this->Password) == $this->Password){
            return true;
        }
        return false;
    }

    public static function findByUsername($username)
    {        
        return static::findOne(['Username' => $username]);
    }

    public static function findByEmail($email)
    {        
        return static::findOne(['Email' => $email]);
    }

    /** INCLUDE USER LOGIN VALIDATION FUNCTIONS**/
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

     /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return "CREATE-AUTH";
    }

     /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

     /**
     * @inheritdoc
     */
    /* modified */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return null;
    }
}
